<aside id="secondary-1" class="sidebar widget-area">
    <div id="nav_menu-2" class="widget widget_nav_menu">
        <h3 class="widget-title">Danh mục sản phẩm</h3>
        <div class="menu-menu-top-container">
            <ul id="menu-menu-top" class="menu">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-164"><a
                        href="<?php echo home_url() ?>">Trang chủ</a></li>
                <li
                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-137 current_page_item menu-item-143">
                    <a href="<?php echo home_url() ?>/gioi-thieu'">giới thiệu</a></li>
                <li
                    class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-144">
                    <a href="<?php echo home_url() ?>/shop">Sản phẩm</a>
                </li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-145"><a
                        href="#">Khuyến mãi</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-146"><a
                        href="#">Báo giá</a></li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-147"><a
                        href="<?php echo home_url() ?>/category/tin-tuc/">Tin tức</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a
                        href="<?php echo home_url() ?>/lien-he/">Liên hệ</a></li>
            </ul>
        </div>
    </div>
    <div id="rt_widget_post-2" class="widget rt-widget rt-post-category">
        <h3 class="widget-title">Tin tức</h3> <!-- start file -->
        <!-- no slide -->
        <div class="news-widget no-slide">
            <?php
                        $args = array(
                                'numberposts' => 10,
                                'offset' => 0,
                                'category' => 0,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'include' => '',
                                'exclude' => '',
                                'meta_key' => '',
                                'meta_value' =>'',
                                'post_type' => 'post',
                                'post_status' => 'draft, publish, future, pending, private',
                                'suppress_filters' => true
                        );
                        
                       $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                ?>
            <?php foreach ($recent_posts as &$recent_post) { 
                //     var_dump( $recent_post);
                    ?>


            <div class="featured-post">
                <div class="align-left">
                    <?php
                       if ( has_post_thumbnail($recent_post['ID']) ) {
                        echo get_the_post_thumbnail($recent_post['ID'], 'thumbnail');
                        }
                        else {
                                $tmp = get_bloginfo( 'template_directory' ).'/images/default-image.png';
                        echo "<img width='150' height='150' src=".$tmp." />";
                        }
                        ?>
                </div>
                <a class="news-title"
                    href="<?php echo home_url().'/'.$recent_post['post_name'] ?>"><?php echo $recent_post['post_title'] ?></a>
            </div>
            <?php      } ?>

        </div>
        <!-- has slide -->
    </div>
</aside>