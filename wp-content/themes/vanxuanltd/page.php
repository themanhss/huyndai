<?php get_header(); ?>

<div id="content" class="site-content">


    <div class="container">
        <div class="row">
            <div id="layout" class="clearfix sidebar-left">


                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">

                        <?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>
                        <div class="clear"></div>

                    </main><!-- #main -->
                </div><!-- #primary -->

                <?php get_sidebar(); ?>

            </div><!-- #layout -->
        </div><!-- .row -->
    </div><!-- .container -->




</div>

<?php get_footer(); ?>