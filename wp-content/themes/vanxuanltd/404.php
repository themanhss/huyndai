<?php get_header(); ?>

<div id="content" class="site-content">


    <div class="container">
        <div class="row">
            <header class="page-header" style=" text-align: center;">
                <h1 class="page-title"><?php echo "Trang web không tồn tại!" ?></h1>
            </header>

            <div class="page-wrapper" style=" text-align: center;">
                <div class="page-content">
                    <h2><?php _e( 'This is somewhat embarrassing, isn’t it?', 'twentythirteen' ); ?></h2>
                    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentythirteen' ); ?>
                    </p>
                </div><!-- .page-content -->
            </div><!-- .page-wrapper -->
        </div><!-- .row -->
    </div><!-- .container -->




</div>

<?php get_footer(); ?>