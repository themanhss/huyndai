<?php get_header(); ?>

<div id="content" class="site-content">

    <!-- MetaSlider -->
    <div style="width: 100%;" class="ml-slider-3-6-7 metaslider metaslider-nivo metaslider-12 ml-slider">

        <div id="metaslider_container_12">
            <div class='slider-wrapper theme-default'>
                <div class='ribbon'></div>
                <div id='metaslider_12' class='nivoSlider'>
                    <img src="http://maudep.com.vn/maudep/hyundaidongdo/wp-content/uploads/2018/01/slider-1600x620.png"
                        height="620" width="1600" alt="" class="slider-12 slide-135" />
                    <img src="http://maudep.com.vn/maudep/hyundaidongdo/wp-content/uploads/2018/01/slider2-1290x500.png"
                        height="620" width="1600" alt="" class="slider-12 slide-136" />
                </div>
            </div>

        </div>
    </div>
    <!--// MetaSlider-->
    <div class="container">
        <div class="row">
            <div id="layout" class="clearfix no-sidebar">


                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">

                        <div class="product_home clear">
                            <div class="product_list clear">
                                <h2 class="heading">
                                    <a href="http://maudep.com.vn/maudep/hyundaidongdo/danh-muc/san-pham-noi-bat/">
                                        Sản phẩm nổi bật </a>
                                </h2>
                                <div class="home-product">
                                    <ul class="woocommerce product-style product_style_1">
                                        <?php  
										$args = array(
											'post_type'      => 'product',
											'posts_per_page' => 8,
										);

										$loop = new WP_Query( $args );

										while ( $loop->have_posts() ) : $loop->the_post();
											global $product; 
											//echo '<br /><a href="'.get_permalink().'">' . woocommerce_get_product_thumbnail().' '.get_the_title().'</a>';
											?>
                                        <li
                                            class="post-158 product type-product status-publish has-post-thumbnail product_cat-khuyen-mai product_cat-san-pham product_cat-san-pham-noi-bat product_cat-san-pham-uu-dai product_cat-xe-thuong-mai instock shipping-taxable purchasable product-type-simple">
                                            <div class="product_item">
                                                <!-- <div class='rt-thumb'>
                                                    <a class='rt-tooltip' href='<?php echo get_permalink(); ?>'><img
                                                            width="300" height="203"
                                                            src=<?php echo woocommerce_get_product_thumbnail() ?>
                                                            class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="2" sizes="(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px" /></a>
                                                </div> -->
                                                <div class='rt-thumb'>

                                                    <a class='rt-tooltip'
                                                        href='<?php echo get_permalink(); ?>'><?php echo woocommerce_get_product_thumbnail('related-thumb', 150, 101) ?></a>
                                                </div>
                                                <div class="content-products">
                                                    <h2 class="rt_woocommerce-loop-product__title"><a
                                                            style="text-transform: uppercase; "
                                                            href="<?php echo get_permalink(); ?>"><?php echo get_the_title()  ?></a>
                                                    </h2>
                                                    <p class="price">
                                                        <?php if( $product->is_on_sale() ) { ?>
                                                        <del> <span>Giá NY
                                                                <?php echo wc_price($product->get_regular_price()); ?></span></del>
                                                        <?php } ?>

                                                        <ins style="color: #fff;">
                                                            Giá :
                                                            <?php if( $product->is_on_sale() ) { echo 'KM '. wc_price($product->get_sale_price());}else{ echo 'NY '. wc_price($product->get_regular_price()); } ?>
                                                        </ins></p>
                                                    <div class="rt_add_to_cart clearfix">
                                                        <a href="<?php echo get_permalink(); ?>"
                                                            class="view_product single_view_product">Chi tiết</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </li>
                                        <?php
										endwhile;

										wp_reset_query();
									?>

                                    </ul>
                                </div>
                            </div>
                            <div class="product_list clear">
                                <h2 class="heading">
                                    <a href="http://maudep.com.vn/maudep/hyundaidongdo/danh-muc/san-pham-uu-dai/">
                                        Sản phẩm ưu đãi </a>
                                </h2>
                                <div class="home-product">
                                    <ul class="woocommerce product-style product_style_1">
                                        <?php  
										$args = array(
											'post_type'      => 'product',
											'posts_per_page' => 8,
										);

										$loop = new WP_Query( $args );

										while ( $loop->have_posts() ) : $loop->the_post();
											global $product; 
											//echo '<br /><a href="'.get_permalink().'">' . woocommerce_get_product_thumbnail().' '.get_the_title().'</a>';
											?>
                                        <li
                                            class="post-158 product type-product status-publish has-post-thumbnail product_cat-khuyen-mai product_cat-san-pham product_cat-san-pham-noi-bat product_cat-san-pham-uu-dai product_cat-xe-thuong-mai instock shipping-taxable purchasable product-type-simple">
                                            <div class="product_item">
                                                <!-- <div class='rt-thumb'>
                                                    <a class='rt-tooltip' href='<?php echo get_permalink(); ?>'><img
                                                            width="300" height="203"
                                                            src=<?php echo woocommerce_get_product_thumbnail() ?>
                                                            class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="2" sizes="(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px" /></a>
                                                </div> -->
                                                <div class='rt-thumb'>

                                                    <a class='rt-tooltip'
                                                        href='<?php echo get_permalink(); ?>'><?php echo woocommerce_get_product_thumbnail('related-thumb', 150, 101) ?></a>
                                                </div>
                                                <div class="content-products">
                                                    <h2 class="rt_woocommerce-loop-product__title"><a
                                                            style="text-transform: uppercase; "
                                                            href="<?php echo get_permalink(); ?>"><?php echo get_the_title()  ?></a>
                                                    </h2>
                                                    <p class="price">
                                                        <?php if( $product->is_on_sale() ) { ?>
                                                        <del> <span>Giá NY
                                                                <?php echo wc_price($product->get_regular_price()); ?></span></del>
                                                        <?php } ?>

                                                        <ins style="color: #fff;">
                                                            Giá :
                                                            <?php if( $product->is_on_sale() ) { echo 'KM '. wc_price($product->get_sale_price());}else{ echo 'NY '. wc_price($product->get_regular_price()); } ?>
                                                        </ins></p>
                                                    <div class="rt_add_to_cart clearfix">
                                                        <a href="<?php echo get_permalink(); ?>"
                                                            class="view_product single_view_product">Chi tiết</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </li>
                                        <?php
										endwhile;

										wp_reset_query();
									?>
                                    </ul>
                                </div>
                            </div>
                            <div class="product_list clear">
                                <h2 class="heading">
                                    <a href="http://maudep.com.vn/maudep/hyundaidongdo/danh-muc/xe-thuong-mai/">
                                        Xe thương mại </a>
                                </h2>
                                <div class="home-product">
                                    <ul class="woocommerce product-style product_style_1">
                                        <?php  
										$args = array(
											'post_type'      => 'product',
											'posts_per_page' => 8,
										);

										$loop = new WP_Query( $args );

										while ( $loop->have_posts() ) : $loop->the_post();
											global $product; 
											//echo '<br /><a href="'.get_permalink().'">' . woocommerce_get_product_thumbnail().' '.get_the_title().'</a>';
											?>
                                        <li
                                            class="post-158 product type-product status-publish has-post-thumbnail product_cat-khuyen-mai product_cat-san-pham product_cat-san-pham-noi-bat product_cat-san-pham-uu-dai product_cat-xe-thuong-mai instock shipping-taxable purchasable product-type-simple">
                                            <div class="product_item">
                                                <!-- <div class='rt-thumb'>
                                                    <a class='rt-tooltip' href='<?php echo get_permalink(); ?>'><img
                                                            width="300" height="203"
                                                            src=<?php echo woocommerce_get_product_thumbnail() ?>
                                                            class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="2" sizes="(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px" /></a>
                                                </div> -->
                                                <div class='rt-thumb'>

                                                    <a class='rt-tooltip'
                                                        href='<?php echo get_permalink(); ?>'><?php echo woocommerce_get_product_thumbnail('related-thumb', 150, 101) ?></a>
                                                </div>
                                                <div class="content-products">
                                                    <h2 class="rt_woocommerce-loop-product__title"><a
                                                            style="text-transform: uppercase; "
                                                            href="<?php echo get_permalink(); ?>"><?php echo get_the_title()  ?></a>
                                                    </h2>
                                                    <p class="price">
                                                        <?php if( $product->is_on_sale() ) { ?>
                                                        <del> <span>Giá NY
                                                                <?php echo wc_price($product->get_regular_price()); ?></span></del>
                                                        <?php } ?>

                                                        <ins style="color: #fff;">
                                                            Giá :
                                                            <?php if( $product->is_on_sale() ) { echo 'KM '. wc_price($product->get_sale_price());}else{ echo 'NY '. wc_price($product->get_regular_price()); } ?>
                                                        </ins></p>
                                                    <div class="rt_add_to_cart clearfix">
                                                        <a href="<?php echo get_permalink(); ?>"
                                                            class="view_product single_view_product">Chi tiết</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </li>
                                        <?php
										endwhile;

										wp_reset_query();
									?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class='img-hot'><img
                                src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/uploads/2018/01/hotline.png'>
                        </div>
                        <div class="clear"></div>
                        <!-- End Product -->
                        <div class="news-home clear">
                            <div class="content-new-left">
                                <img src="http://maudep.com.vn/maudep/hyundaidongdo/wp-content/uploads/2018/01/imgcate.png"
                                    alt="" /> </div>
                            <div class="news-list">
							<?php
                       $args = array( 
						   'posts_per_page' => 3, 
						   'date_query' => array(
									array(
										'year'  => get_the_date('Y', $pid),
										'month' => get_the_date('m', $pid)
									),
								), 
							'order'=> 'ASC', 
							'orderby' => 'title' );
                       $postslist = get_posts( $args, ARRAY_A );
                ?>
            <?php foreach ($postslist  as $post ) { 
				//     var_dump( $recent_post);
				setup_postdata( $post );
                    ?>

                                <div class="box-content-new">
                                    <div class="date-time">
                                        <span><?php echo 'T'. get_the_date('m', $pid); ?></span>
                                        <span><?php echo get_the_date('d', $pid); ?></span>
                                    </div>
                                    <div class="meta-content">
                                        <a class="title"
                                            href="<?php echo the_permalink(); ?>"
                                            title="<?php echo the_title(); ?>"><?php echo the_title(); ?></a>
                                        <p><?php echo get_the_excerpt() ?><a class='more-link' href='<?php echo the_permalink(); ?>'></a>
                                        </p>
                                    </div>
                                </div>
							<?php      } ?>
                            </div>
                            <!--News-List-->
                        </div>

                    </main><!-- #main -->
                </div><!-- #primary -->


            </div><!-- #layout -->
        </div><!-- .row -->
    </div><!-- .container -->




</div><!-- #content -->


<?php get_footer(); ?>