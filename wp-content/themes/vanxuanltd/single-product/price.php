<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<h1 class="product_title entry-title"><?php echo $product->get_title() ?></h1>
<div class="rt_woocommerce_single_product_summary clearfix">
    <div class="">
        <p class="price">
            <span class="rt_price_text">Giá bán:</span>
            <span class="rt_single_regular_price"><?php echo wc_price($product->get_regular_price()); ?></span>
			<?php if( $product->is_on_sale() ) { ?>
            	<span class="rt_single_sale_price"><?php echo wc_price($product->get_sale_price()); ?></span>
			<?php } ?>
        </p>

		<!-- <p class="price"><?php echo $product->get_price_html(); ?></p> -->
    </div>
</div>
