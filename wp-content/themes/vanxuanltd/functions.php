<?php
// Add scripts and stylesheets
function startwordpress_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );
	wp_enqueue_style( 'blog', get_template_directory_uri() . '/css/blog.css' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
}

add_action( 'wp_enqueue_scripts', 'startwordpress_scripts' );

// Thêm phần widget ở chân trang
add_action('woocommerce_single_product_summary', function() {
	//template for this is in storefront-child/woocommerce/single-product/product-attributes.php
	global $product;
	echo $product->list_attributes();

	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
	}, 10);

	function vanxuanltd_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Footer 1', 'vanxuanltd' ),
			'id'            => 'footer-sidebar-1',
			'before_widget' => '<div id="%1$s" class="classic-text-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	
		register_sidebar( array(
			'name'          => __( 'Footer 2', 'vanxuanltd' ),
			'id'            => 'footer-sidebar-2',
			'before_widget' => '<div id="%1$s" class="classic-text-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	
		register_sidebar( array(
			'name'          => __( 'Footer 3', 'vanxuanltd' ),
			'id'            => 'footer-sidebar-3',
			'before_widget' => '<div id="%1$s" class="classic-text-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Copyright', 'vanxuanltd' ),
			'id'            => 'footer-sidebar-4',
			'before_widget' => '<div id="%1$s" class="classic-text-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
	add_action( 'widgets_init', 'vanxuanltd_widgets_init' );

// Loại bỏ phần add to cart
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );