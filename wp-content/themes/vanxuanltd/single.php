<?php get_header(); ?>
<div id="content" class="site-content">
    <div class="container">
        <div class="row">
            <div id="layout" class="clearfix sidebar-left">


                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">

                        <?php 
			if ( have_posts() ) : while ( have_posts() ) : the_post();
			get_template_part( 'content-single', get_post_format() );
			
			// if ( comments_open() || get_comments_number() ) :
			// 		comments_template();
			// 	endif;
				
endwhile; endif; 
			?>
                        <div class="clear"></div>

                    </main><!-- #main -->
                </div><!-- #primary -->

                <?php get_sidebar(); ?>

            </div><!-- #layout -->
        </div><!-- .row -->
    </div><!-- .container -->




</div>

<?php get_footer(); ?>