<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#" class="no-js">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png"
        href="http://maudep.com.vn/maudep/hyundaidongdo/wp-content/uploads/2017/09/logo.png" />
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <script>
    (function(html) {
        html.className = html.className.replace(/\bno-js\b/, 'js')
    })(document.documentElement);
    </script>
    <title>VẠN XUÂN LTD</title>

    <!-- This site is optimized with the Yoast SEO plugin v6.1 - https://yoa.st/1yg?utm_content=6.1 -->
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="VẠN XUÂN LTD" />
    <meta property="og:site_name" content="VẠN XUÂN LTD" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="VẠN XUÂN LTD" />
    <script type='application/ld+json'>
    {
        "@context": "http:\/\/schema.org",
        "@type": "WebSite",
        "@id": "#website",
        "url": "http:\/\/maudep.com.vn\/maudep\/hyundaidongdo\/",
        "name": "HYUNDAI \u0110\u00d4NG \u0110\u00d4",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "http:\/\/maudep.com.vn\/maudep\/hyundaidongdo\/?s={search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
    </script>
    <!-- / Yoast SEO plugin. -->

    <link rel='dns-prefetch' href='//code.jquery.com' />
    <link rel="alternate" type="application/rss+xml" title="Dòng thông tin VẠN XUÂN LTD &raquo;"
        href="http://maudep.com.vn/maudep/hyundaidongdo/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi VẠN XUÂN LTD &raquo;"
        href="http://maudep.com.vn/maudep/hyundaidongdo/comments/feed/" />
    <link rel='stylesheet' id='dashicons-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/css/dashicons.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='menu-icons-extra-css' href='<?php echo plugins_url(); ?>/menu-icons/css/extra.min.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='support-css-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/support.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rt-blog-shortcode-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/rt-blog-shortcode.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='contact-form-7-css'
        href='<?php echo plugins_url(); ?>/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-layout-css'
        href='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css'
        href='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css'
        type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css'
        href='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/css/woocommerce.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='wp-pagenavi-css' href='<?php echo plugins_url(); ?>/wp-pagenavi/pagenavi-css.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/bootstrap.min.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='font-awesome.min.css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/font-awesome.min.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='slick-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/slick.min.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='jquery-ui-base-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/jquery-ui.css' type='text/css'
        media='all' />
    <link rel='stylesheet' id='xzoom-css' href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/xzoom.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='rt-widget-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/widget.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rt-media-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/media.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rt-woo-css' href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/woo.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='rt-main-css'
        href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/main.css' type='text/css' media='all' />
    <style id='rt-main-inline-css' type='text/css'>
    @media (min-width: 1200px) {
        .site-branding .container {
            width: 100% !important;
        }

        .site-branding .container img {
            width: 100%;
        }
    }

    body {}

    .main-navigation,
    .fixed-nav-menu .main-navigation {
        background-image: url(http://maudep.com.vn/maudep/hyundaidongdo/wp-content/uploads/2018/01/menu.png);
    }

    .widget-title {
        background-color: #3464dc;
    }

    #primary-menu li ul.sub-menu {}

    .primary-menu li a:hover,
    .primary-menu li.current-menu-item a {}
    </style>
    <link rel='stylesheet' id='rt-res-css' href='<?php echo get_bloginfo( 'template_directory' );?>/assets/css/res.css'
        type='text/css' media='all' />
    <script type='text/javascript' src='<?php echo get_bloginfo( 'template_directory' );?>/assets/js/md5.js'></script>
    <script type='text/javascript' src='<?php echo get_bloginfo( 'template_directory' );?>/assets/js/util.js'></script>
    <script type='text/javascript' src='<?php echo get_bloginfo( 'template_directory' );?>/assets/js/jquery.js'>
    </script>
    <script type='text/javascript'
        src='<?php echo get_bloginfo( 'template_directory' );?>/assets/js/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='<?php echo get_bloginfo( 'template_directory' );?>/assets/js/xzoom.js'></script>
    <link rel='https://api.w.org/' href='http://maudep.com.vn/maudep/hyundaidongdo/wp-json/' />
    <noscript>
        <style>
        .woocommerce-product-gallery {
            opacity: 1 !important;
        }
        </style>
    </noscript>
</head>

<body class="home blog hfeed has-sidebar">
    <div class="site site-container full w1200">
        <header class="site-header" role="banner">


            <div class="site-branding">
                <div class="container">
                    <div class="row">
                        <a href="<?php echo home_url() ?>" title="VẠN XUÂN LTD">
                            <img src="http://maudep.com.vn/maudep/hyundaidongdo/wp-content/uploads/2018/01/banner.png"
                                alt="VẠN XUÂN LTD">
                        </a>
                        <h1 class="site-title hidden"><a href="#">VẠN XUÂN LTD</a></h1>
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .site-branding -->

            <nav id="site-navigation" class="main-navigation">
                <div class="container">
                    <div class="row">

                        <div class="primary-menu-container visible-lg col-lg-12">
                            <ul id="primary-menu" class="primary-menu menu clearfix">
                                <li id="menu-item-164"
                                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-164">
                                    <a href="<?php echo home_url() ?>">Trang chủ</a></li>
                                <li id="menu-item-143"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143"><a
                                        href='<?php echo home_url() ?>/gioi-thieu'>giới thiệu</a></li>
                                <li id="menu-item-144"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-144">
                                    <a href='<?php echo home_url() ?>/shop'>Sản phẩm</a>
                                </li>
                                <li id="menu-item-145"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-145">
                                    <a href="#">Khuyến mãi</a></li>
                                <li id="menu-item-146"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-146"><a
                                        href="#">Báo giá</a></li>
                                <li id="menu-item-147"
                                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-147"><a
                                        href="<?php echo home_url() ?>/category/tin-tuc/">Tin tức</a></li>
                                <li id="menu-item-19"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a
                                        href="<?php echo home_url() ?>/lien-he/">Liên hệ</a></li>
                            </ul>
                        </div>

                        <div class="hidden-lg col-xs-12">

                            <div id="menu-toggle" class="mobile-menu-no-search">
                                <span id="">Menu</span>
                                <button id="" type="button" class="rt-navbar-toggle hidden-lg">
                                    <span class="screen-reader-text sr-only">Toggle navigation</span>
                                    <span class="icon-bar bar1"></span>
                                    <span class="icon-bar bar2"></span>
                                    <span class="icon-bar bar3"></span>
                                </button>
                            </div>
                        </div>

                    </div><!-- .row -->
                </div><!-- .container -->
            </nav><!-- #site-navigation -->

        </header><!-- #masthead -->