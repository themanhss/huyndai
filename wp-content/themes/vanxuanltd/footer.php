<footer class="site-footer">


    <div class="container">

        <div class="footer-row row clear">
            <div id="footer-1" class="rt-footer footer-1 widget_wrap">
                <div id="rt-widget-text-2" class="widget rt_textwidget">
                <?php
                    if(is_active_sidebar('footer-sidebar-1')){
                        dynamic_sidebar('footer-sidebar-1');
                    }
                ?>
                </div>
            </div>
            <div id="footer-2" class="rt-footer footer-2 widget_wrap">
                <div id="facebook-like-widget-2" class="widget facebook_like">
                <?php
                    if(is_active_sidebar('footer-sidebar-2')){
                            dynamic_sidebar('footer-sidebar-2');
                        }
                ?>
                </div>
            </div>
            <div id="footer-3" class="rt-footer footer-3 widget_wrap">
                <div id="rt-widget-text-3" class="widget rt_textwidget">
                    <?php
                        if(is_active_sidebar('footer-sidebar-3')){
                            dynamic_sidebar('footer-sidebar-3');
                        }
                    ?>
                </div>
            </div>
        </div><!-- .bottom-footer -->
        <div class="footer-site-code row clear">
        </div><!-- .bottom-footer -->
        <div class="copyright row clear">
                <?php
                    if(is_active_sidebar('footer-sidebar-4')){
                        dynamic_sidebar('footer-sidebar-4');
                    }
                ?>
            
        </div>
    </div>


</footer><!-- footer -->
<div class="site-footer-home">
    <div class="container">
    </div>
</div>


<div class="mobile-menu-container">
    <div class="close-menu">Đóng menu <i class="fa fa-times" aria-hidden="true"></i></div>

    <ul id="moblie-menu" class="mobile-menu">
        <li
            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-164">
            <a href="http://maudep.com.vn/maudep/hyundaidongdo/">Trang chủ</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143"><a
                href="http://maudep.com.vn/maudep/hyundaidongdo/gioi-thieu/">giới thiệu</a></li>
        <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-144">
            <a href="http://maudep.com.vn/maudep/hyundaidongdo/danh-muc/san-pham/">Sản phẩm</a>
            <ul class="sub-menu">
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-22"><a
                        href="http://maudep.com.vn/maudep/hyundaidongdo/tin-tuc/">Tin tức</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a
                        href="http://maudep.com.vn/maudep/hyundaidongdo/trang-mau/">Trang Mẫu</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107"><a
                        href="http://maudep.com.vn/maudep/hyundaidongdo/trang-mau/">Trang Mẫu</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a
                        href="http://maudep.com.vn/maudep/hyundaidongdo/tai-khoan/">My account</a></li>
            </ul>
        </li>
        <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-145"><a
                href="http://maudep.com.vn/maudep/hyundaidongdo/danh-muc/khuyen-mai/">Khuyến mãi</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-146"><a
                href="http://maudep.com.vn/maudep/hyundaidongdo/bao-gia/">Báo giá</a></li>
        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-147"><a
                href="http://maudep.com.vn/maudep/hyundaidongdo/tin-tuc/">Tin tức</a></li>
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a
                href="http://maudep.com.vn/maudep/hyundaidongdo/lien-he/">Liên hệ</a></li>
    </ul>
</div><!-- .mobile-menu-container -->


<div class="overlay"></div>

<div class="backtotop"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>

</div><!-- #page -->

<link rel='stylesheet' id='metaslider-nivo-slider-css'
    href='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/ml-slider/assets/sliders/nivoslider/nivo-slider.css'
    type='text/css' media='all' property='stylesheet' />
<link rel='stylesheet' id='metaslider-public-css'
    href='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/ml-slider/assets/metaslider/public.css'
    type='text/css' media='all' property='stylesheet' />
<link rel='stylesheet' id='metaslider-nivo-slider-default-css'
    href='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/ml-slider/assets/sliders/nivoslider/themes/default/default.css'
    type='text/css' media='all' property='stylesheet' />
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {
    "apiSettings": {
        "root": "http:\/\/maudep.com.vn\/maudep\/hyundaidongdo\/wp-json\/contact-form-7\/v1",
        "namespace": "contact-form-7\/v1"
    },
    "recaptcha": {
        "messages": {
            "empty": "H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."
        }
    }
};
/* ]]> */
</script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {
    "ajax_url": "\/maudep\/hyundaidongdo\/wp-admin\/admin-ajax.php",
    "wc_ajax_url": "http:\/\/maudep.com.vn\/maudep\/hyundaidongdo\/?wc-ajax=%%endpoint%%",
    "i18n_view_cart": "Xem gi\u1ecf h\u00e0ng",
    "cart_url": "http:\/\/maudep.com.vn\/maudep\/hyundaidongdo\/gio-hang\/",
    "is_cart": "",
    "cart_redirect_after_add": "no"
};
/* ]]> */
</script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'>
</script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'>
</script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'>
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {
    "ajax_url": "\/maudep\/hyundaidongdo\/wp-admin\/admin-ajax.php",
    "wc_ajax_url": "http:\/\/maudep.com.vn\/maudep\/hyundaidongdo\/?wc-ajax=%%endpoint%%"
};
/* ]]> */
</script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'>
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {
    "ajax_url": "\/maudep\/hyundaidongdo\/wp-admin\/admin-ajax.php",
    "wc_ajax_url": "http:\/\/maudep.com.vn\/maudep\/hyundaidongdo\/?wc-ajax=%%endpoint%%",
    "fragment_name": "wc_fragments_d052537f3309bf518d69f45a3c3e1307"
};
/* ]]> */
</script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'>
</script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/themes/RT/assets/js/slick.min.js'></script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/themes/RT/assets/js/owl.carousel.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var rt_main = {
    "tooltip_on_off": "",
    "tooltip_image": "1",
    "tooltip_title": "1",
    "tooltip_price": "1",
    "thumbelina": null
};
/* ]]> */
</script>
<script type='text/javascript' src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/themes/RT/assets/js/main.js'>
</script>
<script type='text/javascript' src='http://maudep.com.vn/maudep/hyundaidongdo/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript'
    src='http://maudep.com.vn/maudep/hyundaidongdo/wp-content/plugins/ml-slider/assets/sliders/nivoslider/jquery.nivo.slider.pack.js'>
</script>
<script type='text/javascript'>
var metaslider_12 = function($) {
    $('#metaslider_12').nivoSlider({
        boxCols: 7,
        boxRows: 5,
        pauseTime: 3000,
        effect: "random",
        controlNav: false,
        directionNav: true,
        pauseOnHover: true,
        animSpeed: 600,
        prevText: "&lt;",
        nextText: "&gt;",
        slices: 15,
        manualAdvance: false
    });
};
var timer_metaslider_12 = function() {
    var slider = !window.jQuery ? window.setTimeout(timer_metaslider_12, 100) : !jQuery.isReady ? window.setTimeout(
        timer_metaslider_12, 1) : metaslider_12(window.jQuery);
};
timer_metaslider_12();
</script>

</body>

</html>