<?php get_header(); ?>

<div id="content" class="site-content">


    <div class="container">
        <div class="row">
            <div id="layout" class="clearfix sidebar-left">


                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">
                        <?php  if ( have_posts() ) : ?>
                        <div class="arc-news">
                            <h1 class="heading"><span>Tin tức</span></h1>
                            <div class="new-list">
                                <?php
 
 // The Loop
 while ( have_posts() ) : the_post(); ?>

                                <div class="news-post">
                                    <h2 class="title"><a href="<?php the_permalink() ?>"
                                            title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                    <div class="date-time">
                                        <span><i class="fa fa-user" aria-hidden="true"></i> By : <a
                                                href="http://maudep.com.vn/maudep/hyundaidongdo/author/adminraothue/"
                                                title="Đăng bởi adminraothue" rel="author">adminraothue</a></span>
                                        <span><i class="fa fa-eye" aria-hidden="true"></i>59 Views</span><span><i
                                                class="fa fa-calendar" aria-hidden="true"></i>05/09/2017</span>
                                    </div>
                                    <div class="img-post">
                                        <a href="<?php the_permalink() ?>"
                                            title="<?php the_title(); ?>">

                                            <img width="300" height="225" src="<?php 
                                                if(has_post_thumbnail()) {
                                                    echo the_post_thumbnail_url(); 
                                                }
                                                
                                                if(!has_post_thumbnail()){
                                                    echo get_bloginfo( 'template_directory' ).'/images/default-image.png'; }
                                                
                                            ?>"
                                                class="attachment-medium size-medium wp-post-image"
                                                alt="<?php the_title(); ?>"">
                                        </a>
                                        <div class="
                                                mask">
                                            <h2><a href="<?php the_permalink() ?>"
                                                    title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                            </h2>

                                            <p></p>
                                            <p><?php echo the_excerpt() ?><a class="link-more"
                                                    href="<?php the_permalink() ?>">Đọc
                                                    thêm »<span class="screen-reader-text">
                                                        “<?php the_title(); ?>”</span></a></p>
                                            <p></p>


                                    </div>
                                </div>

                                <p><?php echo the_excerpt() ?><a class="more-link" href="<?php the_permalink() ?>">Đọc
                                        Thêm</a></p> <a class="read-more" href="<?php the_permalink() ?>"
                                    title="<?php the_title(); ?>">Xem thêm</a>
                            </div>
                            <?php endwhile; 
 
else: ?>
                            <p>Sorry, no posts matched your criteria.</p>


                            <?php endif; ?>
                        </div>
                        <nav>
	<ul class="pager">
		<li><?php next_posts_link( 'Trang trước' ); ?></li>
		<li><?php previous_posts_link( 'Trang sau' ); ?></li>
	</ul>
</nav>
                </div>
                <!--End #news-wrap-->

                </main><!-- #main -->
            </div><!-- #primary -->

            <?php get_sidebar(); ?>

        </div><!-- #layout -->
    </div><!-- .row -->
</div><!-- .container -->




</div>

<?php get_footer(); ?>