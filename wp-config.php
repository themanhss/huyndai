<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'huyndai');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd8a$<)61%H=Mcs0/%3)xg&D3S2Nm^i{goozi_ra}/XV~mriw[!L ^Y&d`J)C-D#!');
define('SECURE_AUTH_KEY',  ':05vRKSxe.x<e&[q]_B0w6trN#o-wU<C];ErLv$Wns@vGc(8_~upm)Fqr`?t{MBf');
define('LOGGED_IN_KEY',    '4=Qen{E-M+Je8t<d4)]X4+Sj8lU]/[5^.Uf>xKuKPE/&w{WZ=KUX[M$hQOeVqBs ');
define('NONCE_KEY',        '58D!~t#fJG41&vmw}[but?jiBYNmn~+C]Y#m1#Vo4[!|Ayf?)%JQ6s)fV)-zn0-c');
define('AUTH_SALT',        '2Q13qH;12~8*|?cmB T%(uK>Um:z>Gp!_DL6J_<mj<}6-wWWmU&!%=.<.55p[xes');
define('SECURE_AUTH_SALT', '-$;3z0>D9kbdhkXK/>F`5I15e]%#PZx~Y~9UY.W.O[A@Vz:HM)m-3HWwPY4t2uu9');
define('LOGGED_IN_SALT',   'Gq~iP}Yk5mM1q(K~9i|%)a;KUq8`sh(fGv4Q7Try%>ct#!R]+?dzMB)?b;~=>~?w');
define('NONCE_SALT',       '`=sI~VcCFO[0uWDt1AcbZh(Qj} kX|R2;FT 4s]m&E?5HGh[a,_vA#EUshd~wSYU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
